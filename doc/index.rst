.. projection documentation master file, created by
   sphinx-quickstart on Fri Dec 15 16:00:56 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Projection documentation
======================================

Welcome to documentation for Projection!


Projection provides a small function that computes the stereographic projection of a point cloud in :math:`{R^N}` to a point cloud in :math:`{R^(N-1)}`:

.. math::

   P(x, y, z, ..., t)  \Rightarrow  P(x', y', z', ...) \text{ where } x' = x/(1-t), \text{ } y' = y/(1-t), \text{ } z'=z/(1-t), \text{ } ...


Example:

.. code-block::

    from projection import stereographic_projection
    import numpy as np

    print(stereographic_projection(np.random.rand(1,4)))
    # array([[ 0.14022471,  0.96360618,  0.94909878]])

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api_reference

