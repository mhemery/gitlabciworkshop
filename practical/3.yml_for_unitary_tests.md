# Modify the `.gitlab-ci.yml` to launch the unitary tests

## 1st version - Projection installation

- Add the following `.gitlab-ci.yml` file at the git repository root in order to
[install the projection module](../README.md#install-projection-with-pip):

```yml
image: python:3.12

tests:
  stage: test
  tags:
    - ci.inria.fr # For using shared runners
    - small       # For selecting small shared runners
  script:
    - pip install -e .
    - echo "Projection is installed"
```

- Add the `.gitlab-ci.yml` file, commit and push your modifications:

```bash
git add .gitlab-ci.yml
git commit -m "First version - Projection installation in CI"
git push
```

- Verify the CI is launched and successful:
    - On the left sidebar in the Gitlab interface, go to **Build** → **Jobs**.
    - Check the job is running.
    - Click on the **Running** icon and verify the logs.
    - See the job is passed.

## 2nd version - Projection tests

- Add the required steps in the `script` section of the `.gitlab-ci.yml` file to [run the tests](../README.md#run-the-tests).

- Add the `.gitlab-ci.yml` file, commit and push your modifications.

- Verify the CI is launched and successful.

## 3rd version - Projection tests coverage report

- Modify how [the tests are run](../README.md#run-the-tests) to produce the
  coverage report in `html` format.

- Provide the coverage report as an artifact by adding in the `.gitlab-ci.yml` file:
```yml
  script:
    - pip install -e .
    - echo "Projection is installed"
    - (...)
  artifacts:
    paths:
      - htmlcov
```

- Verify the CI is launched and successful:
    - On the left sidebar in the Gitlab interface, go to **Build** → **Jobs**.
    - Check the job is passed.
    - Click on the **Passed** icon and verify the logs.
    - On the right sidebar in the Gitlab interface, browse the job artifact.

## 4th version (OPTIONAL)

If you want to go further, you can also provide coverage test report as expected by Gitlab-CI.

This will enable code coverage even for pull requests and tracks improvement and/or regression in the code coverage.

You can take [gitlabci_gallery/testing/pytest-gitlab-reports](https://gitlab.inria.fr/gitlabci_gallery/testing/pytest-gitlab-reports) as an example.

You can see that a badge with the coverage has been added on the main page of the project.

You can appreciate the coverage enhancement on
[this following merge request](https://gitlab.inria.fr/gitlabci_gallery/testing/pytest-gitlab-reports/-/merge_requests/2/diffs) for instance.

To do so on this project:
- On the left sidebar in the Gitlab interface, go to **Settings** → **General**.
- Expand **Badges**.
- Click on the **Add badge** button.
- Fill the following information:
    - Name: `Coverage`
    - Link: `https://gitlab.inria.fr/%{project_path}/-/pipelines/%{default_branch}/latest`
    - Badge Image URL: `https://gitlab.inria.fr/%{project_path}/badges/%{default_branch}/coverage.svg`
- Click on the **Add badge** button.

- Modify `script` section to generate coverage report both in `html` and `xml` formats.

- Add [coverage](https://docs.gitlab.com/16.7/ee/ci/yaml/index.html#coverage)
section to project’s `.gitlab-ci.yml` file to provide test coverage results to
a merge request. Provided regex is used to find the coverage in the tool’s
output.

- Add [reports](https://docs.gitlab.com/ee/ci/yaml/index.html#artifactsreports)
  section to `artifacts` and specify `coverage.xml` for coverage analysis to
  work.

> Here we still keep `artifacts:paths` part as `reports` artifacts are not
  downloadable from the job details page.

```yml
  script:
    - (...)
  coverage: '/^TOTAL.+?(\d+\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    paths:
      - htmlcov
      - coverage.xml
```

## 5th version

Let's introduce another useful feature of GitLab-CI: [the pipelines](https://docs.gitlab.com/ee/ci/pipelines/).

It allows you to organize the different steps of your CI, and particularly if some steps requires to be launched sequentially or can be launched in parallel in order to fasten the CI.

In our practical, we are going to launch the test suite in parallel with python 3.11 and 3.12.
To do so, we are going to launch 2 [jobs](https://docs.gitlab.com/ee/ci/jobs/) (with a different name), but on the same stage name

```yaml
test python 3.12:    # A different job name
  image: python:3.12 # The docker image name with python 3.12 pre-installed
  stage: test        # The same stage name
  tags:
    - ci.inria.fr
    - small
  script:
    - pip install -e .
    - (...)

test python 3.11:    # A different job name
  image: python:3.11 # The docker image name with python 3.11 pre-installed
  stage: test        # The same stage name
  tags:
    - ci.inria.fr
    - small
  script:
    - pip install -e .
    - (...)
```

(OPTIONAL) If you want to go further, you can also [group the jobs](https://docs.gitlab.com/ee/ci/jobs/#group-jobs-in-a-pipeline) by using a specific syntax: Rename `test python 3.12:` job name as `test python 3:12:` and `test python 3.11:` job name as `test python 3:11:`.

On the left sidebar in the Gitlab interface, go to **Build** → **Pipelines**.
Observe in the pipeline how the 2 tests are grouped.


***

[go to next](4.yml_for_doc_build.md)
